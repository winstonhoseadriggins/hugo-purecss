---
author: "DW84 Inc LLC Foundation"
date: 2018-09-28
linktitle: Autobiographical Portfolio
title: Autobiographical Portfolio
weight: 10
---


## Development of Blockchain Database Applications

The beauty of the blockchain is its distributed architecture.
Once A Transaction is verified by the blockchain, A Permanent Digital Signature Will Propogate Throughout Eternity.

## Increase Visibility  

## Establish Brand Recognition

DW84 Inc LLC Foundation Reconciliation Agent primary role is to assist the client in recovery plus discovery of digital assets.

DW84 Inc LLC Foundation Reconciliation Agent secondary role is to assist the client in crafting a digital identity.
